#!/bin/sh

# If any lines fail return the error to the screen
set -e

envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf

# Nginx runs in the background by default
# This runs in the foreground which is standard for Docker
# and guarantees that a failure will kill the container.
nginx -g 'daemon off;'
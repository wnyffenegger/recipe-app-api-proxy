# Recpie App API Proxy

Nginx proxy for our recipe app proxy

## Windows

On Windows Docker can respond with file not found for entrypoint or other files delimited with CRLF
instead of LF. Convert these files to prevent
issues.

Git auto converts files to CRLF which may cause problems periodically as it overwrites manual
changes.

## Usage

### Env Vars

* `LISTEN_PORT`: port that proxy listens on (default 8000)
* `APP_HOST`: hostname of the app to forward requests to (default app)
* `APP_PORT`: port of the app to forward requests to (default 9000)
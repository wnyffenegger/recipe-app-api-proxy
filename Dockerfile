FROM nginxinc/nginx-unprivileged:1-alpine

# Default nginx image gives root privileges to user
# This image (1) is stripped down nginx image
# Alpine additionally is a stripped down image

LABEL maintainer="wnyffenegger"


COPY ./default.conf.tpl /etc/nginx/default.conf.tpl
COPY ./uwsgi_params /etc/nginx/uwsgi_params

ENV LISTEN_PORT=8000
ENV APP_HOST=app
ENV APP_PORT=9000

# Make changes with root privileges
USER root

# Volume containing static content
# and give read permissions for all users
RUN mkdir -p /vol/static
RUN chmod 755 /vol/static

# Create default nginx configuration
# and make nginx user the owner
RUN touch /etc/nginx/conf.d/default.conf
RUN chown nginx:nginx /etc/nginx/conf.d/default.conf

# Copy over entrypoint script
# and make script executable by all users
COPY ./entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

# Rescind root privileges
USER nginx

VOLUME /vol/static

CMD [ "/entrypoint.sh" ]